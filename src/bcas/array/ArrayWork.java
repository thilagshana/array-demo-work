package bcas.array;

import java.lang.reflect.Array;
import java.util.Random;

public class ArrayWork {
	public static void createRanArray(int size, int range) {
		Random Array = new Random();
		int[] randArray = new int[size];
		for (int r = 0; r < size; r++) {
			randArray[r] = Array.nextInt(range);
		}
		printRandomArray(randArray);
	}


	private static void printRandomArray(int[] array) {

		for (int element : array) {

			System.out.print(element + " , ");

		}

	}
}